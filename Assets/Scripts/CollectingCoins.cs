using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectingCoins : MonoBehaviour
{
    public int coins;
    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag=="Coin")
        {
            Debug.Log("Coin collected");
            coins += 1;
            //other.gameObject.SetActive(false);
            Destroy(other.gameObject);
        }
    }
}
