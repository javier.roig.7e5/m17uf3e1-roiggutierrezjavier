using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinGame : MonoBehaviour
{
    private GameObject player;
    void Start()
    {
        player = GameObject.Find("Paco");
    }

    void Update()
    {
        if (player.GetComponent<CollectingCoins>().coins > 13)
        {
            Debug.Log("YOU WIN!!!");
            SceneManager.LoadScene("Playground");
        }
    }
}
